// modules: posix

using Posix;

const string STR_CALIBRAGE = "NDA-CUNH TESTER (minitalk)\n\n";

struct Server{
	int pid_server;
	int fd_in;
}

Server run_server()
{
	Server server = Server();
	int fds[2];

	pipe(fds);
	int pid = fork();

	if (pid == 0)
	{
		close(fds[0]);
		dup2(fds[1], 1);
		execv("../server", {"server"});
		exit(0);
	}
	close(fds[1]);
	server.pid_server = pid;
	server.fd_in = fds[0];
	return (server);
}


void teste(FileStream stream, Server s, string str_teste)
{
	char buf[2];
	string tmp = "";
	bool is_finis = false;
	bool kill = false;
	double timeout_len = str_teste.length;

	int f = fork();
	if (f == 0)
	{
		execv("../client", {"client", s.pid_server.to_string(), str_teste});
		exit(127);
	}
	else
	{
		new Thread<void>("Timeout", () => {
			ulong microseconds;
			double seconds;
			Timer timer = new Timer ();
			while (true)
			{
				usleep(100);
				seconds = timer.elapsed (out microseconds);
				if(is_finis)
					break;
				if (seconds > str_teste.length / 100.0)
				{
					if(is_finis)
						break;
					printf("%f > %d / 100\n", seconds, str_teste.length);
					printf("\033[31mTimeout [%f]\033[0m\n", seconds);
					Posix.kill(f, Posix.Signal.TERM);
					kill = true;
					timer.stop();
					system("killall server");
					exit(0);
					break ;
				}
			}
		});
		waitpid(-f, null , 0);
		while (stream.gets (buf) != null && buf[0] != '@' && kill == false) {
			if(buf[0] != '\0')
				tmp += @"$(buf[0])";
		}
		if (tmp + "@" == str_teste)
			GLib.stderr.printf("\033[32m[OK] \033[0m");
		else
			GLib.stderr.printf("KO \033[93m[%s]\033[0m \033[95m[%s]\n", tmp, str_teste);
		is_finis = true;
	}
}

void teste_empty_line(FileStream stream, Server s)
{
	bool is_finis = false;
	bool kill = false;
	char buf[2];
	string tmp = "";

	int f2 = fork();
	if (f2 == 0)
	{
		execv("../client", {"client", s.pid_server.to_string(), ""});
		exit(127);
	}
	waitpid(f2, null, 0);
	int f = fork();
	if (f == 0)
	{
		execv("../client", {"client", s.pid_server.to_string(), "@"});
		exit(127);
	}
	new Thread<void>("Timeout", () => {
		ulong microseconds;
		double seconds;
		Timer timer = new Timer ();
		while (true)
		{
			usleep(100);
			seconds = timer.elapsed (out microseconds);
			if(is_finis)
				break;
			if (seconds > 3)
			{
				if(is_finis)
					break;
				printf("%f / 3\n", seconds);
				printf("\033[31mTimeout [%f]\033[0m\n", seconds);
				Posix.kill(f, Posix.Signal.TERM);
				kill = true;
				timer.stop();
				exit(0);
				break ;
			}
		}
	});
	waitpid(f, null , 0);
	while (stream.gets (buf) != null && buf[0] != '@' && kill == false) {
		if(buf[0] != '\0')
			tmp += @"$(buf[0])";
	}
	if (tmp == "")
		print("\033[32m[OK] \033[0m");
	else
		GLib.stderr.printf("""KO [%s] != [""]\n""", tmp);
	is_finis = true;
}

void serie(FileStream stream, Server s)
{
	var str_teste = "ABCDEF@";
	ulong microseconds;
	double seconds;

	print("Teste de 5 caractere\n");
	for (var i = 0; i != 5; i++)
		teste(stream, s, str_teste);

	printf("\nchaine vide !\n");
	teste_empty_line(stream, s);


	Timer timer = new Timer ();
	print("\nTeste de 100 caractere\n");
	str_teste = "....................................................................................................@";
	teste(stream, s, str_teste);
	seconds = timer.elapsed (out microseconds);
	print("seconds: [%f]", seconds);
	timer.stop();

	print("\nTeste de 100 caractere 20 fois\n");
	for (var i = 0; i != 20; i++)
		teste(stream, s, str_teste);

	str_teste = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum mauris quam, ut viverra nisl auctor sit amet. Proin volutpat, libero nec sagittis elementum, est ex malesuada ipsum, at blandit tortor lectus sit amet turpis. Sed id consectetur ipsum. Aenean eget tincidunt justo. Ut non justo efficitur, dignissim mauris et, tristique mi. Praesent nec ullamcorper velit. Aliquam purus dui, lacinia in nulla sit amet, suscipit malesuada urna. Nullam pulvinar, lectus lobortis suscipit cras amet.@";
	print("\nTeste de 500 caractere 20 fois\n");
	for (var i = 0; i != 20; i++)
		teste(stream, s, str_teste);
	str_teste = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum mauris quam, ut viverra nisl auctor sit amet. Proin volutpat, libero nec sagittis elementum, est ex malesuada ipsum, at blandit tortor lectus sit amet turpis. Sed id consectetur ipsum. Aenean eget tincidunt justo. Ut non justo efficitur, dignissim mauris et, tristique mi. Praesent nec ullamcorper velit. Aliquam purus dui, lacinia in nulla sit amet, suscipit malesuada urna. Nullam pulvinar, lectus lobortis suscipit cras amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum mauris quam, ut viverra nisl auctor sit amet. Proin volutpat, libero nec sagittis elementum, est ex malesuada ipsum, at blandit tortor lectus sit amet turpis. Sed id consectetur ipsum. Aenean eget tincidunt justo. Ut non justo efficitur, dignissim mauris et, tristique mi. Praesent nec ullamcorper velit. Aliquam purus dui, lacinia in nulla sit amet, suscipit malesuada urna. Nullam pulvinar, lectus lobortis suscipit cras amet.@";
	printf("\n1000 caractere !\n");
	for (var i = 0; i != 5; i++)
		teste(stream, s, str_teste);
	printf("\n20000 caractere:\n");
	teste(stream, s, bytes_20k);

}

void calibre(int pid, FileStream stream)
{
	char buf[2];
	string tmp = "";

	int f = fork();
	if (f == 0)
	{
		execv("../client", {"client", pid.to_string(), STR_CALIBRAGE});
		exit(0);
	}
	else
	{
		while (stream.gets (buf) != null) {
			tmp += @"$(buf[0])";
			if (STR_CALIBRAGE in tmp)
				break ;
			print ((string) buf);
		}
		print("Calibrage reussis.\n\n");
	}
}

bool verify()
{
	if (access("../client", F_OK) != 0)
	{
		printf("\033[96;1m [INFO] \033[0m../client not found");
		return false;
	}
	if (access("../server", F_OK) != 0)
	{
		printf("\033[96;1m [INFO] \033[0m ../server not found");
		return false;
	}
	return true;
}

int	main(string []args)
{
	if(verify())
	{
		Server s = run_server();
		var stream = FileStream.fdopen (s.fd_in, "r");
		calibre(s.pid_server, stream);
		serie(stream, s);
		print("Terminee\n");
		if (kill(s.pid_server, 9) == -1)
		{
			GLib.stderr.printf("[Error]: impossible de tuer server\n");
			waitpid(s.pid_server, null, 0);
		}
	}
	return (0);
}


const string bytes_20k = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis ipsum id enim tempor mattis vitae tempus lectus. Vestibulum auctor justo vitae quam iaculis, vitae facilisis ante convallis. Mauris est tortor, vulputate in erat vel, placerat commodo diam. Vivamus sit amet mollis nisi. Aliquam eget libero tincidunt, dictum justo ut, porttitor tortor. Integer gravida nec lectus eu imperdiet. Proin aliquam varius sollicitudin. Curabitur tellus metus, cursus sed condimentum id, porttitor eget risus. Suspendisse id tellus arcu. Etiam elementum quam vitae leo bibendum imperdiet. Fusce mi leo, congue in nisl quis, porta suscipit lorem. Proin nulla dui, vehicula in justo a, commodo egestas orci. Aenean tincidunt nisi feugiat, cursus nisl et, lobortis augue. Etiam semper nunc euismod arcu scelerisque efficitur. Suspendisse congue dolor non ante imperdiet elementum eu in justo.  Duis at venenatis quam. Donec tempor, justo nec aliquam tempus, quam ipsum interdum augue, nec convallis nunc dolor nec dui. Mauris vehicula elit vitae dui tempus, sit amet imperdiet lectus semper. Vivamus auctor, lectus a consectetur consectetur, tortor tellus viverra massa, iaculis tincidunt orci tortor at velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Pellentesque aliquet nisl sed eleifend sodales. Praesent eget nisi id dolor vehicula pharetra. Curabitur ultricies eleifend odio, nec cursus magna efficitur at. Donec mattis sapien ut quam ultrices, aliquam congue purus egestas. Fusce pellentesque enim lacinia efficitur accumsan. Pellentesque vestibulum, massa non aliquet elementum, magna leo euismod ipsum, feugiat viverra dolor risus ut risus. In interdum lacinia ante, vel maximus leo vestibulum sit amet.  Quisque non sem magna. Nullam vitae semper elit. Morbi faucibus mollis libero volutpat feugiat. Quisque semper ultricies tortor non malesuada. Morbi viverra eu leo vitae scelerisque. Curabitur tincidunt enim id dui aliquet, a consectetur odio blandit. Sed diam ipsum, euismod non tincidunt ac, lobortis quis mauris. In vitae malesuada eros, eget malesuada libero. Suspendisse potenti. Pellentesque vitae orci vitae libero faucibus blandit. Nulla porta nunc eget hendrerit gravida. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris sed tincidunt tellus, a sollicitudin enim.  Phasellus imperdiet dictum arcu a molestie. Nulla eget dapibus massa, at accumsan nunc. Integer elementum at justo quis venenatis. Donec et felis turpis. Etiam vel elit quis sapien fermentum posuere. Morbi diam mauris, condimentum vitae vehicula nec, euismod vitae dui. Nulla elit odio, sodales non consectetur vel, tempor vitae nunc. Mauris lacus augue, porta sed libero tristique, pellentesque lacinia ligula. Praesent at mi aliquet, porta arcu eu, viverra odio.  Ut vel malesuada purus. Suspendisse hendrerit a odio in consequat. Maecenas porta, arcu eu euismod bibendum, lacus felis accumsan ligula, quis lacinia risus neque quis ligula. Aenean feugiat vel lorem eget tempus. Quisque eu magna quis ligula egestas pellentesque at sit amet felis. In pellentesque tempor sapien nec efficitur. Pellentesque vestibulum nisl id ante suscipit vehicula. Nullam eu posuere lectus. Curabitur ac tempor nisl. Integer auctor ornare diam, a fringilla nisl varius vitae. Proin euismod mollis purus id euismod.  Integer neque risus, elementum sit amet metus eget, maximus fermentum tortor. Aliquam erat volutpat. In hac habitasse platea dictumst. Suspendisse et tellus non elit aliquam ullamcorper. Suspendisse elementum, eros vel tristique pharetra, magna nulla luctus justo, efficitur tempor justo leo ac turpis. Ut vitae fringilla urna, in interdum leo. Sed vel vehicula sem. Duis sed feugiat purus, fringilla faucibus dolor. Curabitur sit amet metus ultrices, congue libero in, faucibus ipsum. Nam aliquam libero at enim rutrum, non sodales sapien gravida. Aliquam pulvinar diam ipsum, vitae gravida erat dapibus sit amet. Ut maximus posuere varius.  Vivamus vel quam enim. Integer orci erat, volutpat id accumsan in, molestie id arcu. Morbi diam diam, interdum et erat eu, imperdiet tincidunt arcu. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis dignissim semper est, a consequat nunc ullamcorper ut. Vivamus congue mi dui, nec feugiat sem ornare sed. Curabitur vitae mi augue. Integer porta facilisis odio, eget feugiat orci lobortis sit amet. In interdum, dolor id interdum maximus, lacus leo egestas felis, sed vestibulum libero leo nec mi. Nulla luctus neque vel nisi aliquam lobortis.  Integer sodales, nulla non viverra fringilla, risus nibh elementum magna, vitae efficitur augue nisl a urna. Maecenas consequat nulla vel tristique fringilla. Proin id viverra nisl. Nam sed dolor neque. Maecenas consequat, purus id sollicitudin viverra, diam massa laoreet massa, sit amet sodales purus metus eu nisl. Maecenas at mauris a lectus dapibus accumsan ut at eros. Sed congue tempus mollis. Mauris eu lacus ac enim accumsan euismod. Phasellus quis sagittis nulla.  Curabitur imperdiet rutrum arcu, vitae maximus velit maximus at. Mauris ut feugiat enim. Vestibulum venenatis lacus gravida eleifend consectetur. Nunc imperdiet rhoncus quam eu hendrerit. Nullam scelerisque risus vitae mi ultricies pharetra. Proin quis luctus dui. Duis in gravida dolor. Nullam scelerisque odio eget velit sodales lobortis. Quisque aliquam mattis pellentesque.  Vivamus fringilla, nisl sed consequat posuere, eros turpis elementum lectus, lobortis sagittis eros mauris quis purus. Mauris pellentesque nunc sed mi gravida, in dictum neque convallis. Pellentesque tempus congue blandit. Quisque vestibulum felis dictum vestibulum facilisis. Cras nec placerat mauris. Suspendisse vel ex non dui finibus accumsan. Cras vitae placerat nulla, ut lobortis metus. Donec nec leo ac sapien maximus consequat at et libero. Mauris dignissim interdum dui in condimentum. Nullam placerat felis eu nunc luctus aliquet. Ut at neque sed nisl laoreet vestibulum vel sed risus. Fusce viverra tellus nec est laoreet, et consequat elit consectetur.  Curabitur vel interdum augue. Maecenas porta, magna in efficitur vulputate, nunc nisl fringilla ante, nec elementum nulla nisl a risus. Etiam venenatis risus porta venenatis posuere. Fusce commodo nisl a augue posuere porta. Mauris id nisl at elit gravida scelerisque. Nullam imperdiet turpis quis ex suscipit fermentum. Praesent finibus vitae ipsum a sodales. Integer tellus ante, tincidunt vitae fermentum a, sollicitudin nec nisi.  Proin euismod ut orci sit amet elementum. Fusce ut ex condimentum, imperdiet risus a, hendrerit erat. Pellentesque vitae tempus lorem, vitae mattis est. Phasellus gravida enim ac velit condimentum bibendum. Duis fringilla et turpis fermentum venenatis. Nulla maximus efficitur orci, in euismod nulla. Suspendisse et felis vel eros consectetur ultrices eget id nibh. Morbi vitae lacinia leo. Integer magna nulla, ultrices sed eros quis, ullamcorper scelerisque magna. Nulla facilisi. Quisque quis odio sit amet arcu euismod auctor. Mauris tincidunt mauris nunc, nec tempor odio sodales at. Nulla nec felis turpis. Nam eget suscipit arcu. Vivamus non lobortis metus. Integer est tortor, cursus sit amet luctus a, convallis eget neque.  Maecenas sagittis congue sem. Aliquam mollis lacus ac semper tempor. Nullam scelerisque nisl eget finibus condimentum. Curabitur vel lorem turpis. Suspendisse eget pretium urna. Curabitur ex lacus, tincidunt a malesuada tincidunt, sagittis vel enim. Aenean feugiat odio at odio semper finibus. In cursus metus sed sem cursus, quis rhoncus nisi suscipit. Nulla rhoncus at enim eu gravida. Vestibulum porta purus nisi, a tempor purus imperdiet at. Duis imperdiet viverra molestie. Interdum et malesuada fames ac ante ipsum primis in faucibus.  Mauris condimentum justo nisl, ut dictum dui semper at. In hac habitasse platea dictumst. Morbi eu libero tincidunt, convallis metus eget, semper enim. Suspendisse aliquet ultricies velit vitae eleifend. Phasellus vulputate enim nec ex convallis hendrerit. Fusce cursus turpis vel magna molestie gravida. In sit amet rutrum neque. Sed interdum est eget metus auctor ullamcorper.

Donec aliquet euismod libero et convallis. Duis et sagittis nunc. Integer egestas iaculis malesuada. Nam et elementum massa. Cras a metus mattis, dictum felis in, ultricies ante. Ut elementum nisi eu dictum posuere. Nullam velit quam, tempor iaculis purus at, efficitur rhoncus ante. Aenean iaculis dolor a nibh varius volutpat. Integer venenatis turpis porta nulla vehicula, eget dictum tellus finibus. Curabitur in tortor ante. Curabitur pretium sodales vehicula. Suspendisse imperdiet sit amet dolor in aliquet. Donec vel tristique nulla. Proin lorem purus, dapibus et viverra sagittis, gravida non leo.

Praesent in commodo enim. Mauris scelerisque sapien vel dapibus eleifend. Donec vel metus ac nisl rutrum vulputate. Nulla hendrerit, sapien non lacinia tempus, velit quam porta turpis, ac accumsan urna lacus sed ipsum. Vestibulum at eros eget ipsum rhoncus gravida. Nulla facilisi. Nunc sodales, sapien ac volutpat dictum, ligula tellus aliquam ex, sit amet imperdiet ligula dui sit amet nibh. Ut hendrerit est id facilisis aliquam. Maecenas interdum ligula sapien, ac elementum nisl rhoncus pharetra. Donec lobortis luctus vestibulum. Nullam non vestibulum ligula. Cras aliquam, lorem quis commodo ullamcorper, ex lectus venenatis arcu, ut mattis eros erat ac turpis. Morbi nec velit non leo convallis ornare ut nec nibh.

Integer magna mauris, venenatis sit amet malesuada in, bibendum eget nisi. Ut egestas tempus condimentum. Donec ullamcorper bibendum felis non placerat. Fusce rutrum imperdiet nisl, nec consequat metus lacinia non. Fusce cursus scelerisque pellentesque. Etiam gravida porta ipsum. Praesent sed semper elit. Morbi non arcu tellus. Nam ut molestie turpis. Ut auctor porttitor ligula, id faucibus libero varius a. Nam in arcu pulvinar, sollicitudin erat id, ultrices libero. Sed ac consequat sem, et aliquet est. Mauris tempor congue dolor vel dignissim. Vivamus sapien turpis, mollis id nisi id, vulputate hendrerit ipsum.

Donec non tortor posuere, aliquam tellus eget, ultricies nunc. Mauris commodo pretium risus nec imperdiet. Proin vestibulum nisi efficitur turpis ultrices bibendum vel sed erat. Phasellus iaculis turpis sed ligula pellentesque, at pretium nisl vestibulum. Aliquam sollicitudin est vitae aliquam pretium. Donec gravida enim egestas purus aliquet, nec egestas lectus lacinia. Donec semper nec lorem eget venenatis. Cras efficitur elit dolor, id sollicitudin enim cursus eu. Ut nibh tellus, pellentesque imperdiet blandit et, elementum id dolor. Vestibulum sit amet porttitor felis.

Cras et eros eget odio rutrum ullamcorper non ac erat. Nullam eleifend lorem porta consectetur porttitor. Praesent porttitor quis justo vel dapibus. Aenean magna sem, sagittis a tempus id, luctus non nisi. Duis luctus nibh turpis, ac vulputate ligula sagittis sit amet. Nam ut erat at metus suscipit facilisis vel non dolor. Ut at purus finibus, condimentum sem vel, cursus massa.

Quisque metus dolor, euismod eget libero sit amet, pulvinar bibendum ligula. Suspendisse a elit pharetra, fermentum purus hendrerit, consectetur lacus. Nunc pretium ante elit, sit amet volutpat enim eleifend at. Cras scelerisque arcu a sagittis eleifend. Aenean efficitur velit at purus condimentum, quis volutpat lorem aliquam. In sapien tellus, aliquam eget neque ac, egestas congue sapien. Vestibulum neque nibh, efficitur a ante nec, gravida placerat dui. In vitae erat diam. Etiam posuere sodales nisi, quis faucibus massa faucibus vitae. Phasellus malesuada lobortis dignissim. Ut bibendum pharetra enim, ut vehicula tellus vestibulum vel. Cras at metus semper ante rutrum rutrum.

Vestibulum placerat pulvinar ante sit amet ultrices. Praesent sit amet turpis eu arcu varius vulputate vitae ac velit. Mauris rhoncus mi turpis, a dignissim quam elementum vitae. Praesent a viverra ante, id laoreet nunc. Pellentesque vel nibh sit amet lorem hendrerit malesuada. Aliquam sed nulla eget erat tristique dapibus. Suspendisse maximus nunc eget ligula sagittis vulputate. Ut a ligula tellus. Maecenas lacinia orci eget nibh rhoncus molestie.
Aenean sem urna, luctus id velit et, elementum commodo mi. Fusce non nulla non neque imperdiet finibus vitae eu risus. Integer sit amet justo a tellus rhoncus cursus. Integer gravida magna non ornare lacinia. Curabitur ultrices sem non leo posuere, eget placerat est cursus. Vivamus tincidunt libero sit amet leo aliquam vehicula. Fusce pellentesque elit in lorem tristique, eget malesuada odio lobortis. Donec at quam eget justo tincidunt pretium at vitae orci. Ut convallis nunc sem, quis pellentesque leo pulvinar eu. Vivamus hendrerit blandit consectetur. Nam ut augue ex. Nam vitae nulla erat. Sed finibus eget sapien scelerisque congue. Duis ac metus eros. Aliquam pulvinar lectus in massa varius, vestibulum sodales velit fermentum.  Vivamus blandit rhoncus nunc, ac suscipit ex iaculis vitae. Pellentesque pretium efficitur nisl, in blandit est pharetra ac. Sed suscipit nulla at massa commodo, hendrerit interdum augue scelerisque. Ut at hendrerit felis. Proin rhoncus turpis sit amet nunc faucibus, eu malesuada augue tristique. Aenean viverra, enim sit amet viverra ultricies, dui nunc porta ligula, eu feugiat justo tortor sed leo. Aenean in purus euismod, condimentum enim nec, fringilla risus. Cras id bibendum diam. Vivamus efficitur, enim vitae dignissim luctus, tellus lacus posuere elit, id finibus dui turpis accumsan magna. Donec pharetra rutrum ligula, vitae euismod odio egestas at. Nam eu imperdiet arcu. Integer pellentesque ac lacus a accumsan. Maecenas id tortor vel massa tristique mattis. Etiam nisi mi, volutpat viverra arcu non, dictum varius nibh.  Integer consequat erat eget imperdiet blandit. Vivamus at libero sit amet erat blandit laoreet id suscipit magna. Pellentesque eu neque ut justo convallis scelerisque. In ut augue feugiat leo sollicitudin bibendum. Proin accumsan vel est eu maximus. Vivamus eu lacus pretium, sollicitudin ligula et, efficitur ante. Praesent consectetur massa odio, vitae condimentum arcu eleifend quis. Sed nisl nisi, dignissim vitae nisl eu, hendrerit malesuada mi.  Integer vel nisl vitae justo ullamcorper facilisis pellentesque sed quam. Vestibulum mi lectus, rhoncus vitae velit sit amet, lobortis ornare risus. Fusce felis diam, gravida a placerat sed, consectetur ut urna. Aenean ut enim quis lorem scelerisque porta. Nulla auctor elit sed justo tincidunt, id imperdiet mi accumsan. Nam condimentum erat vitae tortor suscipit, et commodo erat sollicitudin. Suspendisse pharetra maximus dui, ac molestie odio aliquet sed. Nam convallis magna sit amet mauris viverra euismod.  Proin leo erat, laoreet quis sollicitudin nec, egestas eget nisi. Integer in accumsan felis, eget condimentum libero. Morbi condimentum lacus elit, id porta eros mollis et. Cras est augue, posuere id ultricies ut, porttitor eu ante. Morbi tristique odio a magna porttitor, rutrum molestie lacus scelerisque. Sed efficitur varius mauris, non lacinia dolor egestas tincidunt. Maecenas egestas pharetra lectus. Integer cursus, metus in fringilla imperdiet, erat augue elementum leo, vel elementum ex lorem ac velit. Donec dapibus scelerisque turpis, vel pharetra sem aliquet non.  Fusce vel efficitur metus, eget fermentum leo. Integer rutrum vel est eu faucibus. Quisque egestas ligula eget tempus congue. Donec arcu erat, tincidunt nec eleifend rutrum, tincidunt id quam. Quisque aliquam elit id ante vestibulum molestie. Duis at nisi id elit iaculis condimentum sit amet vel diam. Praesent pulvinar eros sed sapien posuere, tempus placerat mi fermentum. Ut at est iaculis, posuere enim nec, pulvinar massa. Duis turpis orci, lobortis at sem vel, iaculis eleifend ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi iaculis dui ut vehicula ornare. Donec arcu sapien, commodo in malesuada in, pulvinar vitae nisi. Morbi ut tincidunt dui. Praesent maximus rutrum magna, non maximus lacus placerat ac.  Ut eget scelerisque justo, a porttitor ex. Ut imperdiet est eu ante dictum laoreet sed vitae diam. Quisque elit leo, semper vel blandit quis, venenatis hendrerit sem. Morbi dignissim, lectus nec tristique tempor, justo nisi pharetra purus, vel pellentesque augue justo nec mauris. Cras nisl tellus, cursus a semper id, tincidunt finibus mauris. Nunc vitae ipsum tincidunt, imperdiet massa vehicula, varius lectus. Vestibulum at posuere magna, eget tempus ex. Sed vel cursus augue. Aenean luctus condimentum felis, vel pulvinar orci fermentum vitae.  Duis iaculis ante at consequat facilisis. Mauris condimentum, sapien ac iaculis sodales, sem justo vulputate lacus, nec placerat purus arcu a diam. Fusce tincidunt blandit lacus, vel gravida nisl aliquet quis. Cras suscipit tincidunt eros facilisis mollis. Sed quam risus, lacinia egestas convallis nec, gravida at augue. Aliquam euismod justo non hendrerit gravida. Cras molestie blandit luctus.  Nullam bibendum sit amet lacus sed tincidunt. Aliquam tempor leo eu leo condimentum, et egestas risus posuere. Maecenas in sapien malesuada, ultrices eros vitae, condimentum diam. Aliquam non velit id arcu porttitor pharetra. Nullam tempus erat id pulvinar euismod. Curabitur eget ex in tellus scelerisque ultricies id non massa. Nulla facilisis, magna vel sodales mattis, lorem mauris accumsan magna, tincidunt iaculis libero dui a lorem. Nunc placerat feugiat massa. Pellentesque in leo aliquet, varius nulla et, lobortis nisi. Pellentesque quis dui odio. Nulla posuere augue eget malesuada dapibus.  Suspendisse eget leo ac felis sagittis finibus id consectetur mauris. Suspendisse efficitur vehicula tellus sit amet tristique. Nulla ut dapibus purus. Fusce sit amet euismod libero, non aliquet turpis. Praesent sem diam, placerat quis auctor eu, elementum nec nunc. Integer eu mollis diam. Praesent metus lectus, egestas et faucibus eget, aliquam ac metus. Suspendisse at dignissim nisi. Nunc sed nunc eget ex venenatis placerat. Etiam eget arcu eu lorem porttitor sollicitudin ut in tortor.  In sed nunc id purus congue feugiat vitae vel erat. Integer ornare dui ipsum, ac vehicula dolor gravida in. Vivamus porttitor elementum ipsum, sed euismod velit malesuada et. Morbi ac mauris eu magna gravida volutpat. Donec blandit turpis id libero viverra, sit amet pulvinar magna convallis. Nunc at condimentum mi. Vestibulum purus nunc, tincidunt nec pharetra vitae, finibus ac felis.  Suspendisse neque libero, blandit ut tempor quis, commodo quis urna. Nullam ac pharetra risus. Vivamus mattis vestibulum porta. Pellentesque faucibus feugiat nulla, nec lobortis diam aliquet ac. In interdum nisl ut ultricies maximus. Vestibulum consectetur vulputate tellus, et tincidunt nibh fermentum sit amet. Nulla facilisi. Aliquam eleifend libero non semper gravida. Nullam dictum justo sit amet nunc malesuada, et laoreet neque venenatis. Sed sed orci nec risus blandit varius in eu enim. Praesent eget urna sed leo convallis tristique vitae quis metus. Mauris eu est nisi. Mauris eget consequat sem. Maecenas ipsum nisi, ultricies nec mattis id, fermentum et mauris. Aliquam euismod dui magna, vitae pulvinar nibh sodales in. Suspendisse sed faucibus lacus.  Fusce et scelerisque justo, in hendrerit quam. Nullam mollis blandit orci nec pellentesque. Duis id odio facilisis, aliquam felis non, scelerisque diam. Morbi in dignissim velit. Nulla a risus hendrerit, faucibus neque eget, interdum arcu. Donec vestibulum elementum lectus, a tempus est fringilla nec. Nam eu eros accumsan, ultrices libero et, venenatis lorem. Aenean pharetra tellus et odio consequat, sit amet cursus dolor dapibus. Morbi eget lacus rhoncus, feugiat sapien eget, scelerisque metus. Nulla lorem leo, tincidunt non metus eget, dignissim lacinia odio. Aenean ac sapien euismod, luctus metus sit amet, tempor orci. In lectus libero, volutpat nec convallis in, consequat sit amet lectus. Morbi porttitor porttitor.@""";
